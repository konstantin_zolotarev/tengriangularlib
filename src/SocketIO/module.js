'use strict';

var module = angular.module('Tengri.SocketIo', []);

module.factory('SocketIo', ['$rootScope', function($rootScope) {
    var socket;
    try {
        /**
         * Initializing new connection
         */
        socket = io();
    } catch (err) {
        console.log('ERROR: socket.io encountered a problem:\n\n' + err);
    }

    return {
        /**
         * Socket.IO service
         *
         * @link https://github.com/LearnBoost/socket.io-client
         */
        socket: socket,

        /**
         * List of unique event names
         *
         * @type {Array}
         */
        _uniqEvents: [],

        /**
         * Add new Unique listener for event.
         *
         * @param {string} eventName
         * @param {function=} [callback]
         */
        uniqOn: function(eventName, callback) {
            if (socket.listeners(eventName) && socket.listeners(eventName).length > 0) {
                console.log('Event name: '+eventName+' already binded.');
                return;
            }
            //adding event to list of unique event lists
            this._uniqEvents.push(eventName);
            //Binding listener
            this.on(eventName, callback);
        },

        /**
         * Add new listener to event. <br/>
         *
         * If uniq event already binded this method wouldn't add new event listener. Be careful.
         *
         * @param {string} eventName
         * @param {function=} [callback]
         */
        on: function (eventName, callback) {
            //check unique event name existance
            if (!this._uniqEvents.indexOf(eventName)) {
                console.log('Error binding event ! Event "' + eventName +'" was binded as uniq.');
                return;
            }
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },

        /**
         * Adds a one time listener for the event.
         * This listener is invoked only the next time the event is fired, after which it is removed.
         *
         * @param {string} eventName
         * @param {function=} [callback]
         */
        once: function(eventName, callback) {
            socket.once(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },

        /**
         * Emit new event to Socket.io
         *
         * @param {string} eventName
         * @param {*} data
         * @param {function=} callback
         */
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        },

        /**
         * Remove a listener from the listener array for the specified event.
         * Caution: changes array indices in the listener array behind the listener.
         *
         * @param {string} eventName
         * @param {function} listener
         * @returns {*}
         */
        removeListener: function(eventName, listener) {
            if (~this._uniqEvents.indexOf(eventName)) {
                this._uniqEvents.splice(this._uniqEvents.indexOf(eventName), 1);
            }
            return socket.removeListener(eventName, listener);
        },

        /**
         * Removes all listeners, or those of the specified event. <br/>
         * It's not a good idea to remove listeners that were added elsewhere in the code,
         * especially when it's on an emitter that you didn't create (e.g. sockets or file streams). <br/>
         *
         * @param {string} eventName
         * @returns {object} emitter, so calls can be chained.
         */
        removeAllListeners: function (eventName) {
            if (~this._uniqEvents.indexOf(eventName)) {
                this._uniqEvents.splice(this._uniqEvents.indexOf(eventName), 1);
            }
            return socket.removeAllListeners(eventName);
        },

        /**
         * Returns an array of listeners for the specified event.
         *
         * @param {string} eventName
         * @returns {Array}
         */
        listeners: function(eventName) {
            return socket.listeners(eventName);
        }
    };
}]);